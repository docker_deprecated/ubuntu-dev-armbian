# ubuntu-dev-armbian

# [ubuntu-x64-dev-armbian](https://hub.docker.com/r/forumi0721ubuntux64build/ubuntu-x64-dev-armbian/)
[![](https://images.microbadger.com/badges/version/forumi0721ubuntux64build/ubuntu-x64-dev-armbian.svg)](https://microbadger.com/images/forumi0721ubuntux64build/ubuntu-x64-dev-armbian "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721ubuntux64build/ubuntu-x64-dev-armbian.svg)](https://microbadger.com/images/forumi0721ubuntux64build/ubuntu-x64-dev-armbian "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721ubuntux64build/ubuntu-x64-dev-armbian.svg?style=flat-square)](https://hub.docker.com/r/forumi0721ubuntux64build/ubuntu-x64-dev-armbian/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721ubuntux64build/ubuntu-x64-dev-armbian.svg?style=flat-square)](https://hub.docker.com/r/forumi0721ubuntux64build/ubuntu-x64-dev-armbian/)



----------------------------------------
#### Description

* Distribution : [Ubuntu](https://www.ubuntu.com/)
* Architecture : x64
* Appplication : -
    - Armbian kernel development environment
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -i -t --rm \
           -e RUN_USER_NAME=<user_name> \
           forumi0721ubuntux64/ubuntu-x64-dev-armbian:latest
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| RUN_USER_NAME      | login username (default:forumi0721)              |



----------------------------------------
* [forumi0721ubuntux64/ubuntu-x64-dev-armbian](https://hub.docker.com/r/forumi0721ubuntux64/ubuntu-x64-dev-armbian/)

