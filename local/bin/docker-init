#!/bin/sh

set -e

LC_ALL=C

MACHINE=$(uname -m)

docker-install -r -u -c su-exec sudo vim build-essential software-properties-common apt-transport-https wget git dialog bash-completion fakeroot kmod m4 bison flex

wget -qO - https://www.aptly.info/pubkey.txt | apt-key add -
cat << EOF > /etc/apt/sources.list.d/aptly.list
deb http://repo.aptly.info/ squeeze main
EOF
docker-install -c acl

if [ "${MACHINE}" != "$(echo ${MACHINE} | sed -e "s/arm//g")" -o "${MACHINE}" = "aarch64" ]; then
	qemu=
	if [ "${MACHINE}" != "$(echo ${MACHINE} | sed -e "s/arm//g")" ]; then
		qemu=qemu-arm-static
	elif [ "${MACHINE}" = "aarch64" ]; then
		qemu=qemu-aarch64-static
	fi
	if [ ! -z "${qemu}" -a -e /usr/bin/${qemu} ]; then
		cp /usr/bin/${qemu} /usr/local/bin/
		chmod 4755 /usr/local/bin/${qemu}

		cat << EOF > /usr/local/bin/sudo
#!/bin/sh

qemu=/usr/local/bin/${qemu}
sudo=/usr/bin/sudo

if grep -q -w lm /proc/cpuinfo ; then
	if [ "\$(basename "\${0}")" = "sudoedit" ]; then
		\${qemu} -execve -0 /bin/sh \${sudo} -e "\${@}"
	else
		\${qemu} -execve -0 /bin/sh \${sudo} "\${@}"
	fi
else
	if [ "\$(basename "\${0}")" = "sudoedit" ]; then
		\${sudo} -e "\${@}"
	else
		\${sudo} "\${@}"
	fi
fi

exit \$?
EOF
		chmod 4755 /usr/local/bin/sudo
		ln -s sudo /usr/local/bin/sudoedit
	fi
fi

mkdir -p /etc/sudoers.d
ln -sf /usr/local/etc/sudoers /etc/sudoers.d/

mkdir -p /etc
if [ -z "$(grep 'commonrc' /etc/bash.bashrc)" ]; then
	echo '[ -r /usr/local/etc/commonrc  ] && . /usr/local/etc/commonrc' >> /etc/bash.bashrc
fi

mkdir -p /etc/vim
if [ -z "$(grep 'vimrc.local' /etc/vim/vimrc)" ]; then
	echo "source /usr/local/etc/vim/vimrc.local" >> /etc/vim/vimrc
fi
line_start=$(grep -n 'if \[ -z "${debian_chroot:-}" \] \&\& \[ -r /etc/debian_chroot \]; then' /etc/skel/.bashrc | cut -f 1 -d ':')
line_end=
if [ ! -z "${line_start}" ]; then
	for line_num in $(grep -n 'fi' /etc/skel/.bashrc | cut -f 1 -d ':')
	do
		if [ ${line_num} -gt ${line_start} ]; then
			line_end=${line_num}
			break
		fi
	done
fi
if [ ! -z "${line_start}" -a ! -z "${line_end}" ]; then
	sed -i -e "${line_start},${line_end} {; s/^/#/g }" /etc/skel/.bashrc
fi
unset line_start
unset line_end

line_start=$(grep -n 'if \[ "\$color_prompt" = yes \]; then' /etc/skel/.bashrc | cut -f 1 -d ':')
line_end=
if [ ! -z "${line_start}" ]; then
	for line_num in $(grep -n 'fi' /etc/skel/.bashrc | cut -f 1 -d ':')
	do
		if [ ${line_num} -gt ${line_start} ]; then
			line_end=${line_num}
			break
		fi
	done
fi
if [ ! -z "${line_start}" -a ! -z "${line_end}" ]; then
	sed -i -e "${line_start},${line_end} {; s/^/#/g }" /etc/skel/.bashrc
fi
unset line_start
unset line_end

cp -R /etc/skel/. /root/
touch /root/.vimrc

dpkg --add-architecture i386

docker-install -r -u -c git dialog lsb-release binutils wget ca-certificates device-tree-compiler pv bc lzop zip binfmt-support build-essential ccache debootstrap ntpdate gawk gcc-arm-linux-gnueabihf gcc-arm-linux-gnueabi qemu-user-static u-boot-tools uuid-dev zlib1g-dev unzip libusb-1.0-0-dev ntpdate parted pkg-config libncurses5-dev whiptail debian-keyring debian-archive-keyring f2fs-tools libfile-fcntllock-perl rsync libssl-dev nfs-kernel-server btrfs-tools gcc-aarch64-linux-gnu ncurses-term p7zip-full dos2unix dosfstools libc6-dev-armhf-cross libc6-dev-armel-cross libc6-dev-arm64-cross curl gcc-arm-none-eabi libnewlib-arm-none-eabi patchutils python liblz4-tool libpython2.7-dev linux-base swig libpython-dev systemd-container udev distcc libstdc++-arm-none-eabi-newlib gcc-4.9-arm-linux-gnueabihf gcc-4.9-aarch64-linux-gnu g++-4.9-arm-linux-gnueabihf g++-4.9-aarch64-linux-gnu g++-5-aarch64-linux-gnu g++-5-arm-linux-gnueabihf lib32stdc++6 libc6-i386 lib32ncurses5 lib32tinfo5 locales ncurses-base zlib1g:i386 aptly pixz apt-cacher-ng

update-ccache-symlinks

